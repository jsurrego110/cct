# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.shortcuts import redirect
from django.core.urlresolvers import reverse


def index(request):
    return render(request, 'index.html', {})

def trucks(request):
    return render(request, 'trucks.html', {})

def loaderio(request):
    return render(request, 'loaderio.html', {})
